package br.com.treinamento.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.TestRestTemplate;

import br.com.treinamento.constants.Constants;
import br.com.treinamento.dojo.dto.Hero;
import br.com.treinamento.exceptions.HeroAlreadyExists;
import br.com.treinamento.exceptions.HeroNotFound;
import br.com.treinamento.repositories.HeroRepository;
import br.com.treinamento.repositories.impl.HeroRepositoryImpl;

public class Tests {

	TestRestTemplate restTemplate;

	HeroRepository heroRepository = HeroRepositoryImpl.getHeroRepository();

	@Before
	public void before() {
		restTemplate = new TestRestTemplate();
	}

	@After
	public void after() {
		// Testing checking Hulk by ID
		String response = restTemplate.getForObject(Constants.SERVER_URI + Constants.RESET_FAVORITES_LIST,
				String.class);
	}

	@Test
	public void testAddHero() {

		// Testing adding Hulk by ID
		String response = restTemplate.getForObject(
				Constants.SERVER_URI + Constants.ADD_HERO_BY_ID + Constants.PARAMETER_ID + "1009351", String.class);

		assertNotNull(response);

		// Testing checking Hulk by ID
		Hero hero = restTemplate.getForObject(
				Constants.SERVER_URI + Constants.CHECK_HERO_BY_ID + Constants.PARAMETER_ID + "1009351", Hero.class);

		assertNotNull(hero);

	}

	@Test
	public void testAddRepeatedHero() {
		boolean thrown = false;

		try {
			heroRepository.addHero(new Hero(1009351));
			heroRepository.addHero(new Hero(1009351));
		} catch (HeroAlreadyExists e) {
			thrown = true;
		}

		assertTrue(thrown);

		heroRepository.deleteAllHeroes();
	}

	@Test
	public void testRemoveExistentHero() {
		boolean thrown = false;

		try {
			heroRepository.addHero(new Hero(1009351));
			heroRepository.deleteHero(1009351);
		} catch (HeroNotFound e) {
			thrown = true;
		}

		assertFalse(thrown);

		heroRepository.deleteAllHeroes();
	}

	@Test
	public void testRemoveNonExistentHero() {
		boolean thrown = false;

		try {
			heroRepository.deleteHero(1009355);
		} catch (HeroNotFound e) {
			thrown = true;
		}

		assertTrue(thrown);
	}

	@Test
	public void testAddHeroesByName() {
		// Testing adding several heroes that the name starts with Spider.
		String response = restTemplate.getForObject(
				Constants.SERVER_URI + Constants.ADD_HEROES_BY_NAME + Constants.PARAMETER_NAME + "Spider",
				String.class);

		// Testing checking Spider-Man by ID
		Hero hero = restTemplate.getForObject(
				Constants.SERVER_URI + Constants.CHECK_HERO_BY_ID + Constants.PARAMETER_ID + "1009610", Hero.class);

		assertNotNull(hero);

		// Testing checking a Spider-Woman by ID
		hero = restTemplate.getForObject(
				Constants.SERVER_URI + Constants.CHECK_HERO_BY_ID + Constants.PARAMETER_ID + "1010795", Hero.class);

		assertNotNull(hero);

	}

	@Test
	public void testRemoveAllHeroes() {

		heroRepository.addHero(new Hero(1009351));
		heroRepository.addHero(new Hero(1010795));
		heroRepository.addHero(new Hero(1009610));

		heroRepository.deleteAllHeroes();

		assertTrue(heroRepository.getAllHeros().isEmpty());
	}

}
