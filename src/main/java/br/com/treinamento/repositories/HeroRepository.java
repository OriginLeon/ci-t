package br.com.treinamento.repositories;

import java.util.List;

import br.com.treinamento.dojo.dto.Hero;

public interface HeroRepository {

    public List<Hero> getAllHeros();

    public Hero getHero(int id);

    public void deleteHero(int id);
    
    public void deleteAllHeroes();

    public void addHero(Hero hero);
}