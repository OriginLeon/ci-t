package br.com.treinamento.repositories.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.treinamento.dojo.dto.Hero;
import br.com.treinamento.exceptions.HeroAlreadyExists;
import br.com.treinamento.exceptions.HeroNotFound;
import br.com.treinamento.repositories.HeroRepository;

@Component
public class HeroRepositoryImpl implements HeroRepository {
    
	private List<Hero> heroList;
	private static final HeroRepository heroRepository = new HeroRepositoryImpl();
    
    public static HeroRepository getHeroRepository()
    {
    	return heroRepository;
    }

    private HeroRepositoryImpl() {
        heroList = new ArrayList<Hero>();
    }

    public List<Hero> getAllHeros() {
        return heroList;
    }

    public Hero getHero(int id){
        for (Hero emp : heroList) {
            if (emp.getId() == id) {
                return emp;
            }
        }
        throw new HeroNotFound();
    }

    public void deleteHero(int id) {
        for (Hero emp : heroList) {
            if (emp.getId() == id) {
                heroList.remove(emp);
                return;
            }
        }
        throw new HeroNotFound();
    }
    
    public void deleteAllHeroes() {
    	 heroList = new ArrayList<>();
    }

    public void addHero(Hero hero) {
        for (Hero emp : heroList) {
            if (emp.getId() == hero.getId()) {
                throw new HeroAlreadyExists();
            }
        }
        heroList.add(hero);
    }
}