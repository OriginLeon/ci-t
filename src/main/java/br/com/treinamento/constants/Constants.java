package br.com.treinamento.constants;

public class Constants {

	public static final String SERVER_URI = "http://localhost:8080";
	public static final String ADD_HEROES_BY_NAME = "/addHeroesByName";
	public static final String ADD_HERO_BY_ID = "/addHeroById";
	public static final String CHECK_HERO_BY_ID = "/checkHeroById";
	public static final String REMOVE_HERO_BY_ID = "/removeHeroById";
	public static final String RESET_FAVORITES_LIST = "/resetFavoritesList";
	public static final String SHOW_FAVORITES_LIST = "/showFavoritesList";
	public static final String PARAMETER_NAME = "?name=";
	public static final String PARAMETER_ID = "?id=";
	
}

