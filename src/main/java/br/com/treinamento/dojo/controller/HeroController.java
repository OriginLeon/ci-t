package br.com.treinamento.dojo.controller;

import java.util.List;

import javax.ws.rs.Produces;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.treinamento.constants.Constants;
import br.com.treinamento.dojo.dto.Hero;
import br.com.treinamento.dojo.dto.Result;
import br.com.treinamento.dojo.rest.MarvelClient;
import br.com.treinamento.dojo.rest.impl.MarvelClientImpl;
import br.com.treinamento.repositories.HeroRepository;
import br.com.treinamento.repositories.impl.HeroRepositoryImpl;

@RestController
public class HeroController {
	
	// Queria utilizar Autowire mas não tive tempo de configurar
    private MarvelClient marvelClient = MarvelClientImpl.getMarvelClient();
    private HeroRepository heroRepository = HeroRepositoryImpl.getHeroRepository();
	
	@RequestMapping(value = Constants.ADD_HEROES_BY_NAME, method = RequestMethod.GET)
	public String addHeroesByName(@RequestParam("name") String name) throws Exception{
				
				String response = marvelClient.getHeroByName(name);
		
				return mapResponse(response);
	}
	
	@RequestMapping(value = Constants.ADD_HERO_BY_ID, method = RequestMethod.GET)
	public String addHeroesById(@RequestParam("id") int id) throws Exception{
				
				String response = marvelClient.getHeroById(id);
		
				return mapResponse(response);
	}
	
	@RequestMapping(value = Constants.CHECK_HERO_BY_ID, method = RequestMethod.GET)
	@Produces("application/json")
	public Hero checkById(@RequestParam("id") int id) throws Exception{
				
				Hero hero = heroRepository.getHero(id);
		
				return hero;
	}
	
	private String mapResponse(String response) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();				
		Result result = mapper.readValue(response, Result.class);
		
		String heroListString = "Não foi encontrado nenhum heroi com esse nome!";
		
		if(result != null && result.getData() != null && result.getData().getResults() != null)
		{				
			List<Hero> heroList = result.getData().getResults();
			heroListString = "Adicionados heróis: ";
			
			for(Hero hero: heroList)
			{
				heroRepository.addHero(hero);
				heroListString += hero.getName() + ", ";
			}
			
			heroListString = heroListString.substring(0, heroListString.length()-2);
		}
		
		return heroListString;
	}
	
	
	
	@RequestMapping(value = Constants.SHOW_FAVORITES_LIST, method = RequestMethod.GET)
	@Produces("application/json")
	public String showFavoritesList() throws Exception{
				
				List<Hero> heroesList = heroRepository.getAllHeros();
		
				return heroesList.toString();
	}
	
	@RequestMapping(value = Constants.REMOVE_HERO_BY_ID, method = RequestMethod.GET)
	@Produces("application/json")
	public String removeById(@RequestParam("id") int id) throws Exception{
				
				heroRepository.deleteHero(id);
		
				return "Hero was removed!";
	}
	
	@RequestMapping(value = Constants.RESET_FAVORITES_LIST, method = RequestMethod.GET)
	@Produces("application/json")
	public String resetFavoritesList() throws Exception{
				
				heroRepository.deleteAllHeroes();
		
				return "List was reseted";
	}

}
