package br.com.treinamento.dojo.rest.impl;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.binary.Hex;
import org.joda.time.DateTimeUtils;
import org.springframework.stereotype.Component;

import br.com.treinamento.dojo.rest.MarvelClient;
import br.com.treinamento.repositories.HeroRepository;
import br.com.treinamento.repositories.impl.HeroRepositoryImpl;

@Component
public class MarvelClientImpl implements MarvelClient {

	// Chave publica da conta na API da Marvel
	private final String publicKey = "30049e6df2699181c46131e7fe95284d";
	// Chave privada da conta na API da Marvel
	private final String privateKey = "8000d3a7b11ff0ad804c8f82ffcaf838bdb786f6";
	private Long currentTime;
	private Client client = ClientBuilder.newClient();

	private static final MarvelClient marvelClient = new MarvelClientImpl();
	
	public static MarvelClient getMarvelClient()
	{
		return marvelClient;
	}
	
	private MarvelClientImpl() {}

	/* (non-Javadoc)
	 * @see br.com.treinamento.dojo.rest.impl.MarvelClient#getHeroByName(java.lang.String)
	 */
	@Override
	public String getHeroByName(String name) {

		return getHero(name, 0);

	}
	
	@Override
	public String getHeroById(int id) {

		return getHero(null, id);

	}
	
	private String getHero(String name, int id)
	{
		// 1. Criando um client com Jersey
		Client client = ClientBuilder.newClient();

		// 2. Setando o target para o cliente , estabelecendo conexão com a api e
		// buscando seus resultados
		// WebTarget targetClient =
		// cliente.target("http://localhost:8080/RESTFullWebService/backend/usuario");
		WebTarget baseTarget = client.target("http://gateway.marvel.com:80/v1/public/characters");

		if (name != null && name.length() > 0) {
			baseTarget = baseTarget.queryParam("nameStartsWith", name);
		}
		if (id != 0) {
			baseTarget = baseTarget.queryParam("id", id);
		}

		baseTarget = baseTarget.queryParam("ts", getCurrentTime()).queryParam("apikey", publicKey).queryParam("hash",
				getHash());

		Builder builder = baseTarget.request(MediaType.APPLICATION_JSON);

		// 3. Recebendo o retorno da API da Marvel
		return builder.get(String.class);
	
	}

	private long getCurrentTime() {
		currentTime = DateTimeUtils.currentTimeMillis();
		return currentTime;
	}

	private Object getHash() {
				
		// Gerando Hash - necessário para chamada/consumo da API da Marvel
		byte[] hash = org.apache.commons.codec.digest.DigestUtils.md5(currentTime + privateKey + publicKey);

		// Convertendo em string o hash gerado
		return new String(Hex.encodeHex(hash));
	}

	

}