package br.com.treinamento.dojo.rest;

public interface MarvelClient {

	String getHeroByName(String name);
	
	String getHeroById(int id);

}