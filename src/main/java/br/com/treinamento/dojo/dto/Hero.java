package br.com.treinamento.dojo.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Hero implements Serializable {
	
	private int id;

	private String name;
	
	private String description;
	
	public Hero()
	{}
	
	public Hero(int id)
	{
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public String toString()
	{
		if(description == null || description.isEmpty())
		{
			description = "<i>Missing description</i>";
		}
		
		return "ID: <b>" + id + "</b>--> <b>" + name + ":</b> " + description + "<br>"; 
	}
	
}
